Name:       ca_letsencrypt
Version:    2017.11
Release:    1%{?dist}
Summary:    Let's Encrypt Root CA

License:    Public Domain
BuildArch:  noarch

Source:     %{name}-%{version}.tar.gz

%prep
%setup -c -n %{name}-%{version}

%description
Let's Encrypt Root CA.

%package fake 
Summary:    Let's Encrypt Staging Root CA.

%description fake
Let's Encrypt Staging Root CA.
*DO NOT INSTALL THIS ON PRODUCTION MACHINES*.

%install
mkdir -p "%{buildroot}%{_sysconfdir}/grid-security/certificates/"

cp -Pp letsencrypt/* "%{buildroot}%{_sysconfdir}/grid-security/certificates/"
cp -Pp fakeletsencrypt/* "%{buildroot}%{_sysconfdir}/grid-security/certificates/"

%files
%dir %attr(0755,root,root) %{_sysconfdir}/grid-security/certificates/
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/letsencryptauthorityx3.pem
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/letsencryptauthorityx3.signing_policy
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/isrgrootx1.pem
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/isrgrootx1.signing_policy
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/DSTRootCAX3.pem
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/DSTRootCAX3.signing_policy
%{_sysconfdir}/grid-security/certificates/4f06f81d.0
%{_sysconfdir}/grid-security/certificates/4f06f81d.signing_policy
%{_sysconfdir}/grid-security/certificates/4042bcee.0
%{_sysconfdir}/grid-security/certificates/4042bcee.signing_policy
%{_sysconfdir}/grid-security/certificates/2e5ac55d.0
%{_sysconfdir}/grid-security/certificates/2e5ac55d.signing_policy

%files fake 
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/fakeleintermediatex1.pem
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/fakeleintermediatex1.signing_policy
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/fakeleroot.pem
%attr(0644,root,root) %{_sysconfdir}/grid-security/certificates/fakeleroot.signing_policy
%{_sysconfdir}/grid-security/certificates/0a3654cf.0
%{_sysconfdir}/grid-security/certificates/0a3654cf.signing_policy
%{_sysconfdir}/grid-security/certificates/5a7a72df.0
%{_sysconfdir}/grid-security/certificates/5a7a72df.signing_policy


%changelog
* Mon Sep 25 2017 Alejandro Álvarez Ayllon <aalvarez@cern.ch> - 2017.09-1
- Initial packaging

