NAME=ca_letsencrypt
SPEC=$(NAME).spec
VERSION=${shell grep '^Version:' $(SPEC) | awk '{print $$2}' }

CWD=${shell pwd}

RPMBUILD=/tmp/rpmbuild
SRPMS=$(CWD)
RPMS=$(CWD)/out

MOCK_CHROOT=epel-7-x86_64
MOCK_FLAGS=--verbose


RPMDEFINES_SRC=--define='_topdir $(RPMBUILD)' \
    --define='_sourcedir $(CWD)' \
    --define='_builddir %{_topdir}/BUILD' \
    --define='_srcrpmdir $(SRPMS)' \
    --define='_rpmdir $(RPMS)'

RPMDEFINES_BIN=--define='_topdir $(RPMBUILD)' \
    --define='_sourcedir %{_topdir}/SOURCES' \
    --define='_builddir %{_topdir}/BUILD' \
    --define='_srcrpmdir $(SRPMS).' \
    --define='_rpmdir $(RPMS)'


all: srpm

clean:
	rm -rfv out
	rm -fv *.rpm
	rm -fv *.log
	rm -rfv "$(RPMBUILD)"

dist: clean
	tar vczf "$(NAME)-$(VERSION).tar.gz" "letsencrypt" "fakeletsencrypt"

$(RPMBUILD):
	mkdir -p "$(RPMBUILD)"

srpm: dist $(SPEC) $(RPMBUILD)
	/usr/bin/rpmbuild --nodeps -bs $(RPMDEFINES_SRC) $(SPEC)

rpm: srpm
	/usr/bin/rpmbuild --rebuild $(RPMDEFINES_BIN) $(NAME)-$(VERSION)-*.src.rpm

mock: srpm
	/usr/bin/mock $(MOCK_FLAGS) -r $(MOCK_CHROOT) $(NAME)-$(VERSION)-*.src.rpm

