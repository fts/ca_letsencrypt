ca\_letsencrypt
===============
Packaging of the Let's Encrypt CA, so it can be used by Grid software (i.e. gfal2).
It generates two rpms:

* **ca\_letsencrypt**, which contains the production certificates
* **ca\_letsencrypt-fake**, which contains the [staging certificates](https://letsencrypt.org/docs/staging-environment/).
These are to be used for test purposes only, and should *not* be installed on production machines.
